from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
    # Implement the function
    i = 0
    max_ite = 0
    step = 1
    if steps == None:
        max_ite = 1
        step = -1

    history = []

    vide = machine["blank"]

    table = machine["table"]

    final_state = machine["final states"]

    current_state = machine["start state"]
    position = 0

    done = 0
    while(current_state != final_state or i < max_ite):
        execution = {}
        execution["state"] = current_state
        execution["reading"] = input_[position]
        execution["position"] = position
        execution["memory"] = input_
        execution["transition"] = table[current_state][input_[position]]
        history.append(execution)
        
        i += step
        action = table[current_state][input_[position]]
        
        if str(type(action)) == "<class 'dict'>":
            for j in range(len(action)):    
                if list(action.keys())[j] == 'L':
                    if position == 0:
                        input_ = vide + input_
                    else:
                        position -= 1
                    current_state = action[list(action.keys())[j]]
                
                elif list(action.keys())[j] == 'R':
                    position += 1
                    if position == len(input_) :
                        input_ = input_ + vide
            
                    if str(type(action)) == "<class 'dict'>":
                        current_state = action[list(action.keys())[j]]
                elif list(action.keys())[j] == 'write':
                    input_ = input_[:position] + action[list(action.keys())[j]] + input_[position+1:]

        if str(type(action)) == "<class 'str'>":
            if action == "R":
                position += 1
                if position == len(input_) :
                    input_ = input_ + vide
            elif action == "L":
                if position == 0:
                    input_ = vide + input_
                else:
                    position -= 1
        if current_state in machine["final states"]:
            done = 1
            break;

    input_ = remove_blank(input_, vide)
    if done:
        return (input_,history,True)   
    else:
        return (input_,history,False)

def remove_blank(input_, vide):
    #remove blank at start
    while(input_[0] == vide):
        input_ = input_[1:]
    #remove blank at end
    while(input_[len(input_)-1] == vide):
        input_ = input_[:-1]
    return input_
